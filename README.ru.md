# Tiny PDF Viewer

Проект предоставляет пример программы для просмотра PDF-документов на основе PDFium.

Главная цель - демонстрация с использованием минимума исходного кода 
для получения работающего и расширяемого приложения.

Статус сборки:
1. example - [![pipeline status](https://gitlab.com/omprussia/examples/TinyPdfViewer/badges/example/pipeline.svg)](https://gitlab.com/omprussia/examples/TinyPdfViewer/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/examples/TinyPdfViewer/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/examples/TinyPdfViewer/-/commits/dev)

## Условия использования и участия

Исходный код проекта предоставляется по [лицензии](LICENSE.BSD-3-Clause.md),
которая позволяет использовать его в сторонних приложениях.

[Соглашение участника](CONTRIBUTING.md) регламентирует права,
предоставляемые участниками компании «Открытая Мобильная Платформа».

Информация об участниках указана в файле [AUTHORS](AUTHORS.md).

[Кодекс поведения](CODE_OF_CONDUCT.md) — это действующий набор правил
компании «Открытая Мобильная Платформа»,
который информирует об ожиданиях по взаимодействию между членами сообщества при общении и работе над проектами.

## Структура проекта

Проект имеет стандартную структуру приложения на базе C++ и QML для ОС Аврора.

* Файл **[ru.auroraos.TinyPdfViewer.pro](ru.auroraos.TinyPdfViewer.pro)**
        описывает структуру проекта для системы сборки qmake.
* Каталог **[icons](icons)** содержит иконки приложения для поддерживаемых разрешений экрана.
* Каталог **[qml](qml)** содержит исходный код на QML и ресурсы интерфейса пользователя.
    * Каталог **[cover](qml/cover)** содержит реализации обложек приложения.
    * Каталог **[icons](qml/icons)** содержит дополнительные иконки интерфейса пользователя.
    * Каталог **[pages](qml/pages)** содержит страницы приложения.
    * Файл **[ru.auroraos.TinyPdfViewer.qml](qml/ru.auroraos.TinyPdfViewer.qml)**
                предоставляет реализацию окна приложения.
* Каталог **[rpm](rpm)** содержит настройки сборки rpm-пакета.
    * Файл **[ru.auroraos.TinyPdfViewer.spec](rpm/ru.auroraos.TinyPdfViewer.spec)**
                используется инструментом rpmbuild.
* Каталог **[src](src)** содержит исходный код на C++.
    * Файл **[main.cpp](src/main.cpp)** является точкой входа в приложение.
* Каталог **[translations](translations)** содержит файлы перевода интерфейса пользователя.
* Файл **[ru.auroraos.TinyPdfViewer.desktop](ru.aurorao.TinyPdfViewer.desktop)**
        определяет отображение и параметры запуска приложения.
        
## Совместимость

Проект совместим с актуальными версиями ОС Аврора.

## Поток работы приложения

- [FilesModel](src/filesmodel.h) предоставляет модель списка PDF-файлов, хранящихся на устройстве.
  Страница [FilesPage](qml/pages/FilesPage.qml) использует SilicaListView для отображения этих файлов.
  
- Список PDF-файлов - это результат выполнения DBus-запроса Tracker3 . 
  Вызов DBus-запроса Tracker3 выполняется в [TrackerQueryWorker](src/trackerqueryworker.h).

- [DBusAdaptor](src/dbusadaptor.h) реализует DBus-сервис для открытия PDF-файлов по внешнему запросу.
  Адаптер активирует сигнал [fileOpenRequested](src/dbusadaptor.h#L20) в ответ на активацию DBus-метода [openFile](src/dbusadaptor.h#L17).
  Сигнал активирует [Application](qml/ru.auroraos.TinyPdfViewer.qml#L49) и отправляет путь к файлу на страницу [FilesPage](qml/pages/FilesPage.qml#L287).

- [ContentPage](qml/pages/ContentPage.qml#L145) использует qml-тип PdfView из плагина ru.omp.amberpdf
  для отображения содержимого PDF-документа. Этот плагин может отображать PDF-документ в горизонтальной и
  вертикальной ориентации, рисовать аннотации, заметки. PdfView поддерживает быструю прокрутку, навигацию,
  закладки, заметки. Он прост в использовании и рекомендуется к применению в ваших приложениях.

## Снимки экранов

![screenshots](screenshots/screenshots.png)

## This document in English

- [README.md](README.md)
