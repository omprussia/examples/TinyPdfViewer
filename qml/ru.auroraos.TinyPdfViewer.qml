// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

ApplicationWindow {
    id: applicationWindow

    readonly property bool largeScreen: Screen.sizeCategory >= Screen.Large
    readonly property int preferredWidthPopup: largeScreen
                                      ? Math.max(Screen.width, Screen.height) * 0.33333333 - Theme.horizontalPageMargin
                                      : Math.min(Screen.width, Screen.height) - Theme.horizontalPageMargin * 2

    objectName: "applicationWindow"
    initialPage: Qt.resolvedUrl("pages/FilesPage.qml")
    cover: Qt.resolvedUrl("cover/DefaultCoverPage.qml")
    allowedOrientations: defaultAllowedOrientations

    Connections {
        onFileOpenRequested: applicationWindow.activate()

        target: dbusAdaptor
    }
}
