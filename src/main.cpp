// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QScopedPointer>
#include <QGuiApplication>
#include <QQuickView>
#include <QQmlContext>

#include <auroraapp.h>

#include "filesmodel.h"
#include "sortmodel.h"
#include "dbusadaptor.h"
#include "fileinfo.h"
#include "applicationsettings.h"

int main(int argc, char *argv[])
{
    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    application->setOrganizationName(QStringLiteral("ru.auroraos"));
    application->setApplicationName(QStringLiteral("TinyPdfViewer"));

    qmlRegisterType<FilesModel>("ru.auroraos.TinyPdfViewer", 1, 0, "FilesModel");
    qmlRegisterType<SortModel>("ru.auroraos.TinyPdfViewer", 1, 0, "SortModel");
    qmlRegisterType<FileInfo>("ru.auroraos.TinyPdfViewer", 1, 0, "FileInfo");
    ApplicationSettings::registerSingleton("ru.auroraos.TinyPdfViewer");

    QScopedPointer<QQuickView> view(Aurora::Application::createView());

    DBusAdaptor dbusAdaptor(view.data());
    view->rootContext()->setContextProperty(QStringLiteral("dbusAdaptor"), &dbusAdaptor);

    view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/ru.auroraos.TinyPdfViewer.qml")));
    view->show();

    return application->exec();
}
