<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AboutPage</name>
    <message>
        <source>About Application</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;В проекте приводятся примеры использования API, позволяющих взаимодействовать с PDF-документами.&lt;/p&gt;
&lt;p&gt;Основная цель - показать не только то, какие функции доступны для работы с этими API, но и то, как правильно их использовать.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>3-Clause BSD License</source>
        <translation>3-Clause BSD License</translation>
    </message>
    <message>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;em&gt;Copyright (c) 2024 Open Mobile Platform LLC&lt;/em&gt;&lt;/p&gt;
                            &lt;p&gt;Redistribution and use in source and binary forms, with or without
                            modification, are permitted provided that the following conditions are met:&lt;/p&gt;
                            &lt;ol&gt;
                            &lt;li&gt;Redistributions of source code must retain the above copyright notice, this
                            list of conditions and the following disclaimer.&lt;/li&gt;
                            &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice,
                            this list of conditions and the following disclaimer in the documentation
                            and/or other materials provided with the distribution.&lt;/li&gt;
                            &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors
                            may be used to endorse or promote products derived from this software
                            without specific prior written permission.&lt;/li&gt;
                            &lt;/ol&gt;
                            &lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &amp;quot;AS IS&amp;quot; AND
                            ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
                            WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
                            DISCLAIMED. IN NO EVENT SHALL OPEN MOBILE PLATFORM LLC OR CONTRIBUTORS BE
                            LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
                            CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
                            GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
                            HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
                            LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
                            OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>ContentPage</name>
    <message>
        <source>Could not open document</source>
        <translation>Не удалось открыть документ</translation>
    </message>
    <message>
        <source>File not found</source>
        <translation>Файл не найден</translation>
    </message>
    <message>
        <source>Loading</source>
        <translation>Загрузка</translation>
    </message>
    <message>
        <source>Scroll pages horizontally</source>
        <translation>Горизонтальная прокрутка страниц</translation>
    </message>
    <message>
        <source>Share</source>
        <translation>Поделиться</translation>
    </message>
</context>
<context>
    <name>DetailsPage</name>
    <message>
        <source>Details</source>
        <translation>Подробности</translation>
    </message>
    <message>
        <source>File path</source>
        <translation>Путь файла</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <source>Last modified</source>
        <translation>Последнее изменение</translation>
    </message>
    <message>
        <source>Page count</source>
        <translation>Количество страниц</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation>Неизвестно</translation>
    </message>
</context>
<context>
    <name>FilesPage</name>
    <message>
        <source>Documents not found</source>
        <translation>Документы не найдены</translation>
    </message>
    <message>
        <source>Tiny PDF Viewer</source>
        <translation>Просмотр PDF</translation>
    </message>
    <message>
        <source>Share</source>
        <translation>Поделиться</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Search documents</source>
        <translation>Найти документы</translation>
    </message>
    <message>
        <source>Hide search field</source>
        <translation>Скрыть поле поиска</translation>
    </message>
    <message>
        <source>Show search field</source>
        <translation>Показать поле поиска</translation>
    </message>
</context>
<context>
    <name>NavigationPage</name>
    <message>
        <source>Go to page</source>
        <translation>Перейти к странице</translation>
    </message>
    <message>
        <source>Enter page number</source>
        <translation>Введите номер страницы</translation>
    </message>
    <message>
        <source>Page number</source>
        <translation>Номер страницы</translation>
    </message>
    <message numerus="yes">
        <source>Document has %n page(s)</source>
        <translation>
            <numerusform>Всего в документе %n страница</numerusform>
            <numerusform>Всего в документе %n страницы</numerusform>
            <numerusform>Всего в документе %n страниц</numerusform>
        </translation>
    </message>
    <message>
        <source>Document navigation</source>
        <translation>Навигация</translation>
    </message>
    <message>
        <source>No chapters</source>
        <translation>Нет глав</translation>
    </message>
    <message>
        <source>Chapter list</source>
        <translation>Список глав</translation>
    </message>
</context>
<context>
    <name>NotePage</name>
    <message>
        <source>Note</source>
        <translation>Заметка</translation>
    </message>
    <message>
        <source>Author name</source>
        <translation>Автор</translation>
    </message>
    <message>
        <source>Content</source>
        <translation>Содержимое</translation>
    </message>
    <message>
        <source>Page</source>
        <translation>Страница</translation>
    </message>
</context>
<context>
    <name>PopupMenuNotes</name>
    <message>
        <source>Show notes</source>
        <translation>Показать заметки</translation>
    </message>
    <message>
        <source>No notes</source>
        <translation>Заметок нет</translation>
    </message>
    <message>
        <source>Page %1</source>
        <translation>Страница %1</translation>
    </message>
    <message>
        <source>Show in document</source>
        <translation>Показать в документе</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <source>Delete note</source>
        <translation>Удалить заметку</translation>
    </message>
</context>
<context>
    <name>SearchPhrasePage</name>
    <message>
        <source>Phrase search</source>
        <translation>Поиск фраз</translation>
    </message>
    <message>
        <source>Find a phrase</source>
        <translation>Найдите фразу</translation>
    </message>
    <message>
        <source>Searching for phrase</source>
        <translation>Поиск фразы</translation>
    </message>
    <message>
        <source>Not found</source>
        <translation>Не найдено</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <source>%1 | </source>
        <translation>%1 | </translation>
    </message>
    <message>
        <source>%1</source>
        <translation>%1</translation>
    </message>
</context>
</TS>
