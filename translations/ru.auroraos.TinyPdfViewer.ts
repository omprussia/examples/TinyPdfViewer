<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutPage</name>
    <message>
        <source>About Application</source>
        <translation>About Application</translation>
    </message>
    <message>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;The project provides examples of using APIs that allow you to interact with PDF documents.&lt;/p&gt;
&lt;p&gt;The main purpose is to show not only what functions are available to work with these APIs, but also how to use them correctly.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>3-Clause BSD License</source>
        <translation>3-Clause BSD License</translation>
    </message>
    <message>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;em&gt;Copyright (c) 2024 Open Mobile Platform LLC&lt;/em&gt;&lt;/p&gt;
                            &lt;p&gt;Redistribution and use in source and binary forms, with or without
                            modification, are permitted provided that the following conditions are met:&lt;/p&gt;
                            &lt;ol&gt;
                            &lt;li&gt;Redistributions of source code must retain the above copyright notice, this
                            list of conditions and the following disclaimer.&lt;/li&gt;
                            &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice,
                            this list of conditions and the following disclaimer in the documentation
                            and/or other materials provided with the distribution.&lt;/li&gt;
                            &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors
                            may be used to endorse or promote products derived from this software
                            without specific prior written permission.&lt;/li&gt;
                            &lt;/ol&gt;
                            &lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &amp;quot;AS IS&amp;quot; AND
                            ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
                            WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
                            DISCLAIMED. IN NO EVENT SHALL OPEN MOBILE PLATFORM LLC OR CONTRIBUTORS BE
                            LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
                            CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
                            GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
                            HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
                            LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
                            OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>ContentPage</name>
    <message>
        <source>Could not open document</source>
        <translation>Could not open document</translation>
    </message>
    <message>
        <source>File not found</source>
        <translation>File not found</translation>
    </message>
    <message>
        <source>Loading</source>
        <translation>Loading</translation>
    </message>
    <message>
        <source>Scroll pages horizontally</source>
        <translation>Scroll pages horizontally</translation>
    </message>
    <message>
        <source>Share</source>
        <translation>Share</translation>
    </message>
</context>
<context>
    <name>DetailsPage</name>
    <message>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <source>File path</source>
        <translation>File path</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Size</translation>
    </message>
    <message>
        <source>Last modified</source>
        <translation>Last modified</translation>
    </message>
    <message>
        <source>Page count</source>
        <translation>Page count</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation>Unknown</translation>
    </message>
</context>
<context>
    <name>FilesPage</name>
    <message>
        <source>Documents not found</source>
        <translation>Documents not found</translation>
    </message>
    <message>
        <source>Tiny PDF Viewer</source>
        <translation>Tiny PDF Viewer</translation>
    </message>
    <message>
        <source>Share</source>
        <translation>Share</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <source>Search documents</source>
        <translation>Search documents</translation>
    </message>
    <message>
        <source>Hide search field</source>
        <translation>Hide search field</translation>
    </message>
    <message>
        <source>Show search field</source>
        <translation>Show search field</translation>
    </message>
</context>
<context>
    <name>NavigationPage</name>
    <message>
        <source>Go to page</source>
        <translation>Go to page</translation>
    </message>
    <message>
        <source>Enter page number</source>
        <translation>Enter page number</translation>
    </message>
    <message>
        <source>Page number</source>
        <translation>Page number</translation>
    </message>
    <message numerus="yes">
        <source>Document has %n page(s)</source>
        <translation>
            <numerusform>Document has %n page</numerusform>
            <numerusform>Document has %n pages</numerusform>
        </translation>
    </message>
    <message>
        <source>Document navigation</source>
        <translation>Navigation</translation>
    </message>
    <message>
        <source>No chapters</source>
        <translation>No chapters</translation>
    </message>
    <message>
        <source>Chapter list</source>
        <translation>Chapter list</translation>
    </message>
</context>
<context>
    <name>NotePage</name>
    <message>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <source>Author name</source>
        <translation>Author</translation>
    </message>
    <message>
        <source>Content</source>
        <translation>Content</translation>
    </message>
    <message>
        <source>Page</source>
        <translation>Page</translation>
    </message>
</context>
<context>
    <name>PopupMenuNotes</name>
    <message>
        <source>Show notes</source>
        <translation>Show notes</translation>
    </message>
    <message>
        <source>No notes</source>
        <translation>No notes</translation>
    </message>
    <message>
        <source>Page %1</source>
        <translation>Page %1</translation>
    </message>
    <message>
        <source>Show in document</source>
        <translation>Show in document</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Edit</translation>
    </message>
    <message>
        <source>Delete note</source>
        <translation>Delete note</translation>
    </message>
</context>
<context>
    <name>SearchPhrasePage</name>
    <message>
        <source>Phrase search</source>
        <translation>Phrase search</translation>
    </message>
    <message>
        <source>Find a phrase</source>
        <translation>Find a phrase</translation>
    </message>
    <message>
        <source>Searching for phrase</source>
        <translation>Searching for phrase</translation>
    </message>
    <message>
        <source>Not found</source>
        <translation>Not found</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <source>%1 | </source>
        <translation>%1 | </translation>
    </message>
    <message>
        <source>%1</source>
        <translation>%1</translation>
    </message>
</context>
</TS>
